import Header from "../../components/Header"
import ListMap from "../../components/ListMap"
const homepage=()=> {
  return (

     <div>
       <Header name="Tommy"/>
       <ul>
         <ListMap myTab={["cola","gin","tonic","burbound"]} />
       </ul>
     </div>

  );
}

export default homepage;
