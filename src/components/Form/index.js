import React from "react";

class Form extends React.Component{
    constructor(props){
        super(props)
        this.state={value:'Tom'};
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

     handleChange(e){
            this.setState({value:e.target.value})
        }
        handleSubmit(e){
            e.preventDeafult();
        }
        
        
        render(){
    return(
       
            <form onSubmit={this.handleSubmit}>
                <input name="name" type="text" value={this.state.value} onChange={this.handleChange}/>
                <input name="send" type="submit"/>
            </form>
    )}
}
export default Form;